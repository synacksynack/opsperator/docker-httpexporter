FROM docker.io/golang:1.15 AS builder

# Apache Exporter image for OpenShift Origin

WORKDIR /go/src/github.com/Lusitaniae/apache_exporter

COPY apache_exporter.go go.* ./

RUN set -x \
    && if test `uname -m` = aarch64; then \
	export GOARCH=arm64; \
    elif test `uname -m` = armv7l; then \
	export GOARCH=arm; \
    else \
	export GOARCH=amd64; \
    fi \
    && env GO15VENDOREXPERIMENT=1 \
	CGO_ENABLED=0 \
	GOOS=linux \
	go build -o apache_exporter apache_exporter.go \
    && cp ./apache_exporter /apache_exporter \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

FROM scratch

LABEL io.k8s.description="Apache Prometheus Exporter Image." \
      io.k8s.display-name="Apache Prometheus Exporter" \
      io.openshift.expose-services="9113:http" \
      io.openshift.tags="prometheus,exporter,apache" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-httpexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.1.0"

COPY --from=builder /apache_exporter /apache_exporter

ENTRYPOINT ["/apache_exporter"]
USER 1001
