# k8s Prometheus Apache Exporter

Forked from https://github.com/Keleir/httpd_exporter

Switched to https://github.com/Lusitaniae/apache_exporter

Build with:

```
$ make build
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```
